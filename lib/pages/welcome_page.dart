import 'package:camping_app/pages/home_page.dart';
import 'package:camping_app/themes/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: getBody(),
    );
  }

  Widget getBody() {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SvgPicture.asset("assets/images/welcome_image.svg"),
            Column(
              children: [
                Text(
                  "Find yourself\noutside",
                  style: appTitle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Book unique camping experiences on over 300,300 campsites cabins, RV packs, public parks and more",
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.8),
                    fontSize: 12,
                    height: 1.5,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(primary),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ))),
              onPressed: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: ((context) => HomePage())));
              },
              child: Container(
                width: double.infinity,
                height: 45,
                child: Center(
                    child: Text(
                  "Explore Now!",
                  style: TextStyle(color: textWhite),
                )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
