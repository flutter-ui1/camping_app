import 'package:camping_app/pages/welcome_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      fontFamily: 'lato'
    ),
    debugShowCheckedModeBanner: false,
    home: WelcomePage(),
  ));
}

