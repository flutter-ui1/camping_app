import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../themes/constant.dart';

class CardDiscover extends StatelessWidget {
  final String imgSrc;
  final String label;
  const CardDiscover({
    Key? key,
    required this.imgSrc,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      width: 140,
      decoration: BoxDecoration(
        color: textWhite,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SvgPicture.asset(imgSrc),
            Text(
              label,
              style: TextStyle(color: textBlack.withOpacity(0.5)),
            )
          ],
        ),
      ),
    );
  }
}
